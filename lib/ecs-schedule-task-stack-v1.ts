import * as cdk from "@aws-cdk/core";
import aas = require("@aws-cdk/aws-applicationautoscaling");
import ecs = require("@aws-cdk/aws-ecs");
import ecs_patterns = require("@aws-cdk/aws-ecs-patterns");
import logs = require("@aws-cdk/aws-logs");

// Using ECS Patterns
export class EcsScheduleTaskStackV1 extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const cluster = new ecs.Cluster(this, "cluster1", {
            clusterName: "cluster1",
        });

        const logGroup = new logs.LogGroup(this, "cluster1-logs", {
            removalPolicy: cdk.RemovalPolicy.DESTROY,
            retention: logs.RetentionDays.ONE_WEEK,
        });
        const logDriver = ecs.LogDrivers.awsLogs({
            streamPrefix: "cluster1",
            logGroup: logGroup,
        });

        const cronOptions: aas.CronOptions = {
            minute: "*",
            hour: "*",
            day: "*",
            month: "*",
            // weekDay: "?", // Cannot supply both 'day' and 'weekDay', use at most one
            year: "*",
        };

        new ecs_patterns.ScheduledFargateTask(this, "ScheduledFargateTask", {
            schedule: aas.Schedule.cron(cronOptions),
            cluster,
            desiredTaskCount: 1,
            scheduledFargateTaskImageOptions: {
                image: ecs.ContainerImage.fromRegistry("bash"),
                command: ["whoami"],
                cpu: 256,
                memoryLimitMiB: 512,
                logDriver,
            },
        });
    }
}
