import * as cdk from "@aws-cdk/core";
import aas = require("@aws-cdk/aws-applicationautoscaling");
import ecs = require("@aws-cdk/aws-ecs");
import events = require("@aws-cdk/aws-events");
import event_targets = require("@aws-cdk/aws-events-targets");
import logs = require("@aws-cdk/aws-logs");

// Not using ECS Patterns
export class EcsScheduleTaskStackV2 extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const cluster = new ecs.Cluster(this, "cluster2", {
            clusterName: "cluster2",
        });

        const logGroup = new logs.LogGroup(this, "cluster2-logs", {
            removalPolicy: cdk.RemovalPolicy.DESTROY,
            retention: logs.RetentionDays.ONE_WEEK,
        });
        const logDriver = ecs.LogDrivers.awsLogs({
            streamPrefix: "cluster2",
            logGroup: logGroup,
        });

        const cronOptions: aas.CronOptions = {
            minute: "*",
            hour: "*",
            day: "*",
            month: "*",
            // weekDay: "?", // Cannot supply both 'day' and 'weekDay', use at most one
            year: "*",
        };

        const taskDef = new ecs.FargateTaskDefinition(this, "taskDefinition", {
            cpu: 256,
            memoryLimitMiB: 512,
        });

        // Container name is task definition and containerOverrides
        // must be same. Otherwise, the scheduled task won't run.
        const containerName = "myContainer";
        taskDef.addContainer(containerName, {
            image: ecs.ContainerImage.fromRegistry("bash"),
            logging: logDriver,
        });

        const rule = new events.Rule(this, "rule", {
            enabled: true,
            schedule: events.Schedule.cron(cronOptions),
        });

        rule.addTarget(
            new event_targets.EcsTask({
                cluster,
                taskDefinition: taskDef,
                taskCount: 1,
                containerOverrides: [
                    {
                        containerName: containerName,
                        command: ["whoami"],
                    },
                ],
            })
        );
    }
}
