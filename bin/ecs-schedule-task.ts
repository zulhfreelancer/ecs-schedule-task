#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "@aws-cdk/core";
import { EcsScheduleTaskStackV1 } from "../lib/ecs-schedule-task-stack-v1";
import { EcsScheduleTaskStackV2 } from "../lib/ecs-schedule-task-stack-v2";

const app = new cdk.App();
new EcsScheduleTaskStackV1(app, "EcsScheduleTaskStackV1");
new EcsScheduleTaskStackV2(app, "EcsScheduleTaskStackV2");
